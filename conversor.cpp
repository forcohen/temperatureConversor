/* Algoritimo desenvolvido na aula de LP1 (linguagem de programação)
/  Autor: Arthur Cohen
/  Curso: Bacharel em Tenologia da Informacao - UFRN
/  04/03/2017
*/

#include "conversor.h"

float f2c(float temp){
    temp = (temp-32.0)/1.8; //formula de conversao f° to c°
    return temp;
}
float c2f(float temp){
    temp=((temp*1.8)+32.0); //formula de conversao c° to f°
    return temp;
}
